# README #

* Main files: 
- checkSchmidlinMostromFit
	- run this to make sure the current model fit reproduces the Schmidlin and Molstrom studies
- fitSchmidlinMostrom
	- run this to re-fit the Schmidlin  and Molstrom studies
You may need to update directories to find appropriate helper files and data files.

helperfiles
- files to simulate, calculate objective function, and plot each study

data files - datasets digitized from papers

papers - key papers
