plotSchmidlinSS = function(theta, inits, beta, cvrsim, A) {
  print("plotSchmidlin")
  #####Simulate Salt resistant arm  
  

  results = simulateSchmidlinSS(theta, inits, beta, SS = FALSE, cvrsim)
  xSR = results$xHS
  xnorm = results$xnorm
  
  xPctChSR = 100*(xSR - xnorm)/xnorm
  xPctChSR$time = xSR$time
  
  xChSR = (xSR - xnorm)
  xChSR$time = xSR$time

  results = simulateSchmidlinSS(theta, inits, beta, SS = TRUE, cvrsim)
  
  xSS = results$xHS
  xnorm = results$xnorm
  
  #Correct for lower baseline hematocrit
  xnorm$hematocrit = xnorm$hematocrit - 2
  xSS$hematocrit = xSS$hematocrit - 2
  
  xPctChSS = 100*(xSS - xnorm)/xnorm
  xPctChSS$time = xSS$time
  
  xChSS = (xSS - xnorm)
  xChSS$time = xSS$time
  
  xSS$Group = "SaltSensitive"
  xSR$Group = "SaltResistant"
  xChSS$Group = "SaltSensitive"
  xChSR$Group = "SaltResistant"
  xPctChSS$Group = "SaltSensitive"
  xPctChSR$Group = "SaltResistant"
  
  x = rbind(xSR, xSS)
  xCh = rbind(xChSR, xChSS)
  xPctCh = rbind(xPctChSR, xPctChSS)
  
  
hCO=ggplot() + geom_path(data=xPctCh, aes(x=time, y = cardiac_output, color = Group)) + 
  geom_point(data=A[A$Variable == "CO" & A$Type == "Output",], aes(x=Time, y = Value, group = Group, color = Group)) + 
  geom_errorbar(data=A[A$Variable == "CO" & A$Type == "Output",], aes(x=Time, ymin = Value - SD, ymax = Value + SD, group = Group, color = Group), alpha = 0.25)


hHct = ggplot() + geom_path(data = x, aes(x=time, y = hematocrit, color = Group)) + 
  geom_point(data=A[A$Variable == "Hematocrit" & A$Type == "Output",], aes(x=Time, y = Value, group = Group, color = Group)) +
  geom_errorbar(data=A[A$Variable == "Hematocrit" & A$Type == "Output",], aes(x=Time, ymin = Value - SD, ymax = Value + SD, group = Group, color = Group), alpha = 0.25)


hMAP=ggplot() + geom_path(data=xPctCh, aes(x=time, y = mean_arterial_pressure_MAP, color = Group)) + 
  geom_point(data=A[A$Variable == "MAP" & A$Type == "Output",], aes(x=Time, y = Value, group = Group, color = Group)) +
  geom_errorbar(data=A[A$Variable == "MAP" & A$Type == "Output",], aes(x=Time, ymin = Value - SD, ymax = Value + SD, group = Group, color = Group), alpha = 0.25)


hSVR = ggplot() + geom_path(data = xPctCh, aes(x=time, y = total_peripheral_resistance, color = Group)) + 
  geom_point(data=A[A$Variable == "SVR" & A$Type == "Output",], aes(x=Time, y = Value, group = Group, color = Group))  +
  geom_errorbar(data=A[A$Variable == "SVR" & A$Type == "Output",], aes(x=Time, ymin = Value - SD, ymax = Value + SD, group = Group, color = Group), alpha = 0.25)



hcumNa = ggplot() + geom_path(data = x, aes(x=time, y=cumNaBalance, color = Group )) +
  geom_point(data = A[A$Variable == "CumNaBalance" & A$Type == "Output",], aes(x=Time, y = Value, group = Group, color = Group) )+
  geom_errorbar(data=A[A$Variable == "CumNaBalance" & A$Type == "Output",], aes(x=Time, ymin = Value - SD, ymax = Value + SD, group = Group, color = Group), alpha = 0.25)


hpg = ggplot() + geom_path(data = xSR, aes(x=time/24/60, y = postglomerular_pressure) )

hmfp = ggplot() + geom_path(data = xSR, aes(x=time/24/60, y = Na_concentration)) + ylim(c(130,150)) + geom_hline(yintercept = xnorm$Na_concentration[1], linetype = 2, color = "gray")
hrvr = ggplot() + geom_path(data = xSR, aes(x=time/24/60, y = GFR_ml_min)) + ylim(c(80,120)) + geom_hline(yintercept = xnorm$GFR_ml_min[1], linetype = 2, color = "gray")

grid.arrange(hMAP, hCO, hSVR, hHct,hcumNa, hpg, hrvr, hmfp, nrow = 4)

}

